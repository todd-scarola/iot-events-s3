'use strict';
const SF = require('../../sharedFunctions.js');

exports.processData = async(event) => {

    // S3 scheme: Y/M/D/gateways/<id>/oem/<messageType>/<time.json>
    var date = new Date(event.device_time * 1000);
    var path = await SF.getS3Path(date);
    path += '/tags/' + event.tag_id + '/pom/' + event.messageType + '/' + (date.getTime() / 1000) + '.json';

    //exports.saveToS3 = async function(bucket, key, metadata, data)
    if (await SF.saveToS3(process.env.S3_BUCKET, path, null, JSON.stringify(event))) {
        return process.env.S3_BUCKET + '/' + path;
    }

};

/*
{
  "messageType": "tagStatus",
  "tag_id": "c8fbd12e804d",
  "event_time": 1605814629,
  "battery_level": 2960,
  "battery_percent": 80,
  "firmware": "00020800",
  "device_time": 1605814819,
  "last_upload_time": 1605814803
}
*/
