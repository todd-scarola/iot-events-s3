'use strict';
const SF = require('../../sharedFunctions.js');

exports.processData = async(event) => {

    // S3 scheme: Y/M/D/gateways/<id>/oem/<messageType>/<time.json>
    var date = new Date(event.eventTime);
    var path = await SF.getS3Path(date);
    path += '/gateways/' + event.gatewayId + '/pom/status/'+ (date.getTime() / 1000) + '.json';
    
    //exports.saveToS3 = async function(bucket, key, metadata, data)
    if (await SF.saveToS3(process.env.S3_BUCKET, path, null, JSON.stringify(event))) {
        return process.env.S3_BUCKET + '/' + path;
    }

};

/*
{
  "messageType": "gatewayStatus",
  "gatewayId": "354616090324281",
  "eventTime": "2020-11-19T19:23:20.000Z",
  "firmware": 179495,
  "rssi": -88,
  "sinr": 23
}
*/
