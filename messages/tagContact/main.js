'use strict';
const SF = require('../../sharedFunctions.js');

exports.processData = async(event) => {

    // S3 scheme: Y/M/D/gateways/<id>/oem/<messageType>/<time.json>
    var date = new Date(event.event_start * 1000);
    var path = 'POM/tagContact/' + await SF.getS3Path(date) + '/deviceId-' + event.tag_a + '/' + date.getTime() + '.json';

    //exports.saveToS3 = async function(bucket, key, metadata, data)
    var result = await SF.saveToS3(process.env.S3_BUCKET, path, null, JSON.stringify(event));
    if (result) {
        return process.env.S3_BUCKET + '/' + path;
    }
    else {
        console.error('error writing to S3: %j', result);
    }

};

/*
{
  "messageType": "tagContact",
  "tag_a": "e64cfa3180cc",
  "tag_b": "eeef1eec9550",
  "event_start": 1605814719,
  "event_end": 1605814839,
  "range": 2,
  "duration": 120
}
*/
