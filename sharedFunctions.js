const AWS = require('aws-sdk');
const S3 = new AWS.S3();

exports.saveToS3 = async function(bucket, key, metadata, data) {
    try {

        //write data to bucket
        let params = {
            Bucket: bucket, //root bucket
            Key: key, //file name/key
            Body: data, //json to upload,
            ACL: 'bucket-owner-full-control', //access policy,
            ContentType: 'application/json',
            Metadata: metadata
        };

        return await S3.putObject(params).promise();

    }
    // request failed
    catch (ex) {
        console.error(ex);
    }
};

exports.getS3Path = async function(date) {
    return date.getFullYear().toString() + '/' + (date.getMonth() + 1).toString().padStart(2, '0') + '/' + (date.getDate().toString().padStart(2, '0'));
};
